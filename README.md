![Open Cité logo](img/logoAndTitle/banniereOpenCite-2.png)
All in one plugin for creating cité des sable (swagg/nations/éléments/vestiges...).


![How to create a cité with Open Cité](img/logoAndTitle/TitleOpenCite-1.png)

## Install
This plugin need [worldedit](https://www.curseforge.com/minecraft/mc-mods/worldedit), [worldguard](https://www.curseforge.com/minecraft/bukkit-plugins/worldguard) 
- Add the [worldedit](https://www.curseforge.com/minecraft/mc-mods/worldedit) plugin in the plugins folder
- Add the [worldguard](https://www.curseforge.com/minecraft/bukkit-plugins/worldguard) plugin in the plugins folder
- Add the [OpenCité](#) plugin in the plugins folder

Run the minecraft server it will generate the plugins config file

![Configurations files](img/logoAndTitle/TitleOpenCite-2.png)
### Main Configuration
First you can edit the main config file `plugins/openCite/config.yml` sample :
```yml
#######################################
# This file contain the main config   #
#               ###                   #
#                                     #
# The NPCS.yml contain the npc        #
#  informations and trade offer       #
#                                     #
# data folder contain all usefull     #
# data for the plugin. You don't need #
# to edit any file in the data folder #
#######################################

# Plugin language only french for now 
lang: fr # fr
# City Name
cityName: OpenCité
# Your coin name
coinName: openCoin
# Your coin symbol 
coinSymbol: OC
# Teams name
teams: 
- 'red'
- 'green'
- 'blue'
# Teams color in SAME ORDER of teams names
teamsColors: 
- '&4'
- '&2'
- '&1'
```

### Create trades and NPCs
You can create as many **trades** and **NPCs** you want in the `plugins/openCite/NPC.yml` example :

```yml
#######################################
# This file contain the NPC config    #
#               ###                   #
#                                     #
# Here you can create NPCS and trades #
# for your cite.                      #
#                                     #
# See other config in config.yml      #
#######################################
NPCS:
  # Example NPC '0' 
  '0':
    # NPC name with color tag
    realName: '&1&lStone'
    # NPC skin from a Minecraft account 
    skinName: 'Notch'
    # How the price is define (Only classic for now)
    trade_type: 'classic' 
    # List of trade
    trades:
      # Each item is define like below
      # Minecraft_item_name: PRICE
      
      ####### MAXIMUN 54 TRADES PER NPC !!! #######
      GRAVEL: 10
      GRASS: 15
      ACACIA_WOOD: 20
      ANDESITE: 985
      BARREL: 10
      BEACON: 99999
      BEEF: 88
      BELL: 19
      CARROT: 10
      CHEST: 63
      CLAY: 52
      COAL: 12
  # Example NPC '1' 
  '1':
    # NPC name with color tag
    realName: '&1&lBIRCH'
    # NPC skin from a Minecraft account 
    skinName: 'lamisedaxeh'
    # How the price is define (Only classic for now)
    trade_type: 'classic'
    # List of trade
    trades:
      # Each item is define like below
      # Minecraft_item_name: PRICE    
      
      ####### MAXIMUN 54 TRADES PER NPC !!! #######  
      BIRCH_BOAT: 52
      BIRCH_DOOR: 62
      TNT: 999999
#
# You can create as many npc you want. 
# Please respect the table naming of NPC '0', '1', '2', '3' ...
#
```

![In game setup](img/logoAndTitle/TitleOpenCite-3.png)
### NPC spawning
Spawn an NPC (previously setup in the config file above):
![](img/screenshot/spawnNPC.png)

Result :
![](img/screenshot/NPCSpawned.png)

The GUI buy/selling item : 
![](img/screenshot/NPCGUI.png)
![](img/screenshot/NPCGUIWithItem.png)
![](img/screenshot/sellingItemGUI.png)

### Scoreboard management
Spawning the scoreboard :
![](img/screenshot/SpawnScoreboardPlayer.png)
![](img/screenshot/SpawnScoreboardTeam.png)
Result (with one player) :
![](img/screenshot/scoreboard.png)

### House/plot management

First you need to create a world guard region and setup the [flag](https://worldguard.enginehub.org/en/latest/regions/flags/) (security it's important) as you want.
![](img/screenshot/creatingRegion.png)
Next we create the plot in the cité with the **SAME** name as the worldguard region and a price : 
![](img/screenshot/createPlot.png)

And if i want i can buy the plot : 
![](img/screenshot/buyPlot.png)

And here the result in the database file we can see the plot *home1* at a price 15000 owned by lamisedaxeh :

```yml
  '2':
    name: home1
    price: '15000'
    ownerUUID: 2a04fc06-7ee6-49cb-9401-964e9e8c0e74
    ownerName: lamisedaxeh
```


![Todo, contribution and community](img/logoAndTitle/TitleOpenCite-4.png)

See the [developpement board here](https://gitlab.com/lamisedaxeh/opencite/-/boards). All idea, issue or pull request are welcomed :).

