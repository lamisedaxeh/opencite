package com.lamisedaxeh.i18n;

import java.util.ResourceBundle;

import com.lamisedaxeh.ConfigData;


public class I18n2 {
	static ResourceBundle rb = ResourceBundle.getBundle("com/lamisedaxeh/i18n/"+ConfigData.getLang());
	/**
	 * Get text on i18n file with the same name
	 * @param name
	 * @return
	 */
	public static String gs(String name) {
		
		return rb.getString(name) ;
	}
}
