package com.lamisedaxeh.plot;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;

import com.lamisedaxeh.i18n.I18n2;
import com.lamisedaxeh.Main;
import com.lamisedaxeh.sell.Sell;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.RegionContainer;

public class PlotData {

		private static Main plugin;
	    private static File customConfigFile;
	    private static FileConfiguration customConfig;

	    
	    public PlotData(Main plugin) {
	    	PlotData.plugin = plugin;
	    	createCustomConfig();
		}
	  //TODO Throw error
	    public FileConfiguration getCustomConfig() {
	        return PlotData.customConfig;
	    }

	    private void createCustomConfig() {
	        customConfigFile = new File(plugin.getDataFolder(), "data/home-data.yml");
	        if (!customConfigFile.exists()) {
	            customConfigFile.getParentFile().mkdirs();
	            plugin.saveResource("data/home-data.yml", false);
	        }
	        customConfig= new YamlConfiguration();
	        try {
	            customConfig.load(customConfigFile);
	        } catch (IOException | InvalidConfigurationException e) {
	            e.printStackTrace();
	        }
	    }
	    
	    
	    public static boolean createPlot(String plotName, String args) {
			RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
			RegionManager regions = container.get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
			
			if(regions.hasRegion(plotName)) {
				int key = customConfig.getConfigurationSection("plots").getKeys(false).size();

				customConfig.set("plots." + key + ".name", plotName);
				customConfig.set("plots." + key + ".price", args);
				saveConfig();			
				return true;
			}
			return false;
	    }
	    
	    
	    
	    public static boolean buyPlot(String plotName, Player player) {
			if(player.hasPermission("opencite.player.action.buyplot")) {
				RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
				RegionManager regions = container.get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
				
				if(regions.hasRegion(plotName)) {
    				for (String idPlot: customConfig.getConfigurationSection("plots").getKeys(false)) {
    					if(plotName.equalsIgnoreCase(customConfig.getString("plots." + idPlot + ".name"))) {
    						
    				    	if(hasOwner(plotName)) {
    				    		player.sendMessage(I18n2.gs("log.t.plotHasOwne"));
    				    		return false;
    				    	}else {
    				    		int price = getPrice(plotName);
    			    			if(Sell.buy("plot", plotName, price, player)) {
		    						customConfig.set("plots." + idPlot + ".ownerUUID",player.getUniqueId().toString());
		    						customConfig.set("plots." + idPlot + ".ownerName",player.getName());
		    	    				saveConfig();
		    	    				regions.getRegion(plotName).getOwners().addPlayer(player.getUniqueId());
		    	    				return true;
    			    			}else{
    			    				player.sendMessage(I18n2.gs("log.t.noMoney"));
    			    				return false;
    			    			}
    			    			
    				    	}
    					}
    				}
				}
				return false;
			}else {
				player.sendMessage(I18n2.gs("log.t.deniedPermission"));
				return true;
			}
	    }
	    
	    @SuppressWarnings("null")//TODO Throw error
		private static boolean hasOwner(String plotName) {
			for (String idPlot: customConfig.getConfigurationSection("plots").getKeys(false)) {
				if(plotName.equalsIgnoreCase(customConfig.getString("plots." + idPlot + ".name"))) {
					if(customConfig.getString("plots." + idPlot + ".ownerUUID") == null ) {
						return false;
					}else {
						return true;
					}
				}
			}
			return (Boolean) null;
		}
	    
	    @SuppressWarnings("null")//TODO Throw error
		private static int getPrice(String plotName) {
			for (String idPlot: customConfig.getConfigurationSection("plots").getKeys(false)) {
				if(plotName.equalsIgnoreCase(customConfig.getString("plots." + idPlot + ".name"))) {
					return customConfig.getInt("plots." + idPlot + ".price");
				}
			}
	    	return (Integer) null;
		}
	    
	    
		public static ArrayList<String> getPlots() {
			ArrayList<String> result = new ArrayList<>();
			for (String idPlot: customConfig.getConfigurationSection("plots").getKeys(false)) {
				result.add(customConfig.getString("plots." + idPlot + ".name"));
				
			}
			return result;
		}
	    

	    //TODO refactor ALL function below :
		
		/**
		 * Write data to the file on HDD
		 */
		public static void saveConfig() {
			if (PlotData.customConfig == null || PlotData.customConfigFile == null)
				return;
			
			try {
				getConfig().save(PlotData.customConfigFile);
			} catch (IOException e) {
				plugin.getLogger().log(Level.SEVERE,"Could not save config to" + PlotData.customConfigFile,e);
			}
		}
		
		/**
		 * @return Actual config 
		 */
		public static FileConfiguration getConfig() {
			if (PlotData.customConfig == null)
				reloadConfig();
			return PlotData.customConfig;
		}

		/**
		 * Create file or load data from exiting file
		 */
		private static void reloadConfig() {
			if (PlotData.customConfigFile == null)
				PlotData.customConfigFile = new File(plugin.getDataFolder(), "data/home-data.yml");
			PlotData.customConfig = YamlConfiguration.loadConfiguration(PlotData.customConfigFile);
				
			InputStream defaultStream = plugin.getResource("data/home-data.yml");
			if (defaultStream !=null) {
				YamlConfiguration defaultConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(defaultStream));
				PlotData.customConfig.setDefaults(defaultConfig);
			}
		}
		

}
