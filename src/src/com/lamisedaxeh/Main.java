package com.lamisedaxeh;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.lamisedaxeh.gui.Events.InventoryClickListener;
import com.lamisedaxeh.holoDisplay.HoloData;
import com.lamisedaxeh.npc.DataManager;
import com.lamisedaxeh.npc.NPC;
import com.lamisedaxeh.npc.NPCsFile;
import com.lamisedaxeh.npc.PacketReader;
import com.lamisedaxeh.npc.Events.ClickNPC;
import com.lamisedaxeh.npc.Events.Join;
import com.lamisedaxeh.plot.PlotData;
import com.lamisedaxeh.scoreboard.PersonalScoreboard;
import com.lamisedaxeh.sell.Sell;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;


import net.minecraft.server.v1_16_R3.EntityPlayer;

public class Main extends JavaPlugin implements org.bukkit.event.Listener{

	public static DataManager data;
	
	/**
	 * Setup the plugin on server startup or reload
	 */
	@Override
	public void onEnable() {		
		//Load main config
		new ConfigData(this);
		
		//Load yml saver
		data = new DataManager(this);
		
		//Load commands
		new Commands(this);
		
		new NPCsFile(this);
		new Sell(this);
		new PlotData(this);
		new HoloData(this);
		
		//Add events listener 
		this.getServer().getPluginManager().registerEvents(new Join(this),this);
		this.getServer().getPluginManager().registerEvents(new ClickNPC(),this);
		new InventoryClickListener(this);
		
		//Load NPC from the previous save
		if(data.getConfig().contains("data"))
			loadNPC();
		
		//Inject all player already connected (like on reload)
		if(!Bukkit.getOnlinePlayers().isEmpty())
			for(Player player : Bukkit.getOnlinePlayers()) {
				PacketReader reader = new PacketReader();
				reader.inject(player);
				
				//INIT scoreBoard
				PersonalScoreboard.createBoard(player);
				PersonalScoreboard.start(player, this);
			}
	}

	/**
	 * Unsetup the plugin on server stop or reload
	 */
	@Override
	public void onDisable() {
		//Unload the packet injection
		for(Player player : Bukkit.getOnlinePlayers()) {
			PacketReader reader = new PacketReader();
			reader.unInject(player);
			
			//Remove the NPC to get fresh NPC from the data file when the plugin reload 
			for(EntityPlayer npc : NPC.getNPC())
				NPC.removeNPC(player,npc);
		}
	}

	/**
	 * Hack for avoid static warning ?	
	 * @return
	 */
	public static FileConfiguration getData() {
		return data.getConfig();
	}
	/**
	 * Hack for avoid static warning ?	
	 * @return
	 */
	public static void saveData() {
		data.saveConfig();
	}
	
	
	/**
	 * Load NPC from save file
	 */
	public void loadNPC() {
		FileConfiguration file = data.getConfig();
		
		//Loop over saved NPCs
		data.getConfig().getConfigurationSection("data").getKeys(false).forEach(npc -> {
			//Set create NPC with location/pitch/Yaw/name/...
			Location location = new Location(Bukkit.getWorld(file.getString("data." + npc + ".world")),
					file.getDouble("data." + npc + ".x"),
					file.getDouble("data." + npc + ".y"),
					file.getDouble("data." + npc + ".z")
				);
			location.setPitch((float) file.getDouble("data." + npc + ".p"));
			location.setYaw((float) file.getDouble("data." + npc + ".yaw"));
			
			String name = file.getString("data." + npc + ".name");
		
			GameProfile gameProfile = new GameProfile(UUID.randomUUID(),Utils.chat(name));// MAX 16 char for color code + name !!!!
			gameProfile.getProperties().put("textures",new Property("textures", file.getString("data." + npc + ".texture"), file.getString("data." + npc + ".signature")));
			
			NPC.loadNPC(location, gameProfile,this);
		});
	}
	
	
}
