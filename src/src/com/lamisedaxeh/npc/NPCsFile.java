package com.lamisedaxeh.npc;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.lamisedaxeh.ConfigData;
import com.lamisedaxeh.Main;
import com.lamisedaxeh.Utils;
import com.lamisedaxeh.gui.InventoryGui;
import com.lamisedaxeh.i18n.I18n2;

import net.md_5.bungee.api.ChatColor;

public class NPCsFile {
	private static Main plugin;
    private static File customConfigFile;
    private static FileConfiguration customConfig;

    
    
    public NPCsFile(Main plugin) {
		NPCsFile.plugin = plugin;
		createCustomConfig();
		createGui();
		//createNPC();//TODO
	}

    private void createGui() {
    	customConfig.getConfigurationSection("NPCS").getKeys(false).forEach(npc -> {
    		String name 		= Utils.chat(customConfig.getString("NPCS." + npc + ".realName"));
    		//String skinName 	= customConfig.getString("NPCS." + npc + ".skinName");
    		//String trade_type 	= customConfig.getString("NPCS." + npc + ".trade_type");
    		
    		double nbItem = customConfig.getConfigurationSection("NPCS." + npc + ".trades").getKeys(false).size();
    		
    		//TODO Fix Max size
    		InventoryGui invGui = new InventoryGui(name,(int)Math.ceil(nbItem/9));
    		int slot = 0;
    		
    		for (String item : customConfig.getConfigurationSection("NPCS." + npc + ".trades").getKeys(false)) {
    			int price = customConfig.getInt("NPCS." + npc + ".trades." + item);
    			
//    			invGui.addItem(item, slot, null, "Vendu: X","","Prix: " + price + "€ Nom du Coin");
    			invGui.addItem(item, slot, null, String.format(I18n2.gs("gui.l.price"), " &e"+Utils.formatPrice(price)+" &6"+ConfigData.getCoinSymbol()));
    			slot++;
			}
		});
		
	}
    

	@SuppressWarnings("null")
	public static int getPrice(String material) {//TODO support doublon
    	for (String npc : customConfig.getConfigurationSection("NPCS").getKeys(false)) {
    		for (String item : customConfig.getConfigurationSection("NPCS." + npc + ".trades").getKeys(false)) {
    			if(material.equalsIgnoreCase(item)) {
    				int price = customConfig.getInt("NPCS." + npc + ".trades." + item);
    				return price;
    			}
    		}
		}
    	return (Integer) null;
	}
    
    public static String getRealName(String name) {
    	for (String npc : customConfig.getConfigurationSection("NPCS").getKeys(false)) {
    		if(ChatColor.stripColor(Utils.chat(customConfig.getString("NPCS." + npc + ".realName"))).equalsIgnoreCase(name)) {
    			return customConfig.getString("NPCS." + npc + ".realName");
    		}
		}
    	return null;
	}
    
    public static String getSkinName(String realName) {
    	for (String npc : customConfig.getConfigurationSection("NPCS").getKeys(false)) {
    		String npcRealName = customConfig.getString("NPCS." + npc + ".realName");
    		if(realName.equalsIgnoreCase(npcRealName)) {
    			return customConfig.getString("NPCS." + npc + ".skinName");
    		}
		}
		return null;
    }

	public FileConfiguration getCustomConfig() {
        return NPCsFile.customConfig;
    }

    private void createCustomConfig() {
        customConfigFile = new File(plugin.getDataFolder(), "NPCS.yml");
        if (!customConfigFile.exists()) {
            customConfigFile.getParentFile().mkdirs();
            plugin.saveResource("NPCS.yml", false);
         }

        customConfig= new YamlConfiguration();
        try {
            customConfig.load(customConfigFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }	
}
