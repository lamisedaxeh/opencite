package com.lamisedaxeh.npc;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.lamisedaxeh.Main;
import com.lamisedaxeh.npc.Events.RightClickNPC;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import net.minecraft.server.v1_16_R3.EntityPlayer;
import net.minecraft.server.v1_16_R3.Packet;
import net.minecraft.server.v1_16_R3.PacketPlayInUseEntity;

public class PacketReader {
	Channel channel;
	
	public static Map<UUID, Channel> channels = new HashMap <UUID,Channel>();
	
	/**
	 * Inject a player
	 * @param player
	 */
	public void inject(Player player) {
		CraftPlayer craftplayer = (CraftPlayer) player;
		channel = craftplayer.getHandle().playerConnection.networkManager.channel;
		channels.put(player.getUniqueId(), channel);
		
		if (channel.pipeline().get("PacketInjector") != null)
			return;
		
		//Call readPacket() for all new PacketPlayInUseEntity
		channel.pipeline().addAfter("decoder", "PacketInjector", new MessageToMessageDecoder<PacketPlayInUseEntity>() {
			@Override
			protected void decode(ChannelHandlerContext channel, PacketPlayInUseEntity packet, List<Object> arg) throws Exception {
				arg.add(packet);
				readPacket(player, packet);
			}
		});
	}
	
	/**
	 * Remove the injection from a player
	 * @param player
	 */
	public void unInject(Player player) {
		channel = channels.get(player.getUniqueId());
		if(channel.pipeline().get("PacketInjector") != null)
			channel.pipeline().remove("PacketInjector");
	}
	
	/**
	 * Decode a PacketPlayInUseEntity from a player and react if needed
	 * @param player The player who create the packet
	 * @param packet The packet
	 */
	public void readPacket(Player player, Packet<?> packet) {
		if (packet.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInUseEntity")) {
			if(getValue(packet, "action").toString().equalsIgnoreCase("ATTACK"))
				return;
			if(getValue(packet, "d").toString().equalsIgnoreCase("OFF_HAND"))
				return;
			if(getValue(packet, "action").toString().equalsIgnoreCase("INTERACT_AT"))
				return;
			int id = (int) getValue(packet, "a");
			
			if(getValue(packet, "action").toString().equalsIgnoreCase("INTERACT")) {
				for (EntityPlayer npc : NPC.getNPC()) {
					if (npc.getId() == id) {
						Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getPlugin(Main.class),new Runnable() {
							@Override
							public void run() {
								Bukkit.getPluginManager().callEvent(new RightClickNPC(player, npc));
							}						
						});
					}						
				}
			}
		}
	}
	
	/**
	 * Get value of a field in packet
	 * @param instance the packet where you want to extract a value
	 * @param name name of the field
	 * @return extract value
	 */
	private Object getValue(Object instance, String name) {
		Object result = null;	
		try {
			Field field = instance.getClass().getDeclaredField(name);
			field.setAccessible(true);
			result = field.get(instance);
			field.setAccessible(false);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return result;
	}
}

