package com.lamisedaxeh.npc;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.lamisedaxeh.Main;

/**
 * May need a rework
 * @author lamisedaxeh
 */
public class DataManager {
	private Main plugin;
	/** Yaml data */
	private FileConfiguration dataConfig = null;
	/** The file where saved the config */
	private File configFile = null;
	
	/**
	 * Init the class and create needed file
	 * @param plugin
	 */
	public DataManager(Main plugin) {
		this.plugin = plugin;
		saveDefaultConfig();
	}
	
	/**
	 * Create fil or load data from exiting file
	 */
	private void reloadConfig() {
		if (this.configFile == null)
			this.configFile = new File(this.plugin.getDataFolder(), "data/npcs-data.yml");
		this.dataConfig = YamlConfiguration.loadConfiguration(this.configFile);
			
		InputStream defaultStream = this.plugin.getResource("data/npcs-data.yml");
		if (defaultStream !=null) {
			YamlConfiguration defaultConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(defaultStream));
			this.dataConfig.setDefaults(defaultConfig);
		}
	}
	
	/**
	 * @return Actual config 
	 */
	public FileConfiguration getConfig() {
		if (this.dataConfig == null)
			reloadConfig();
		return this.dataConfig;
	}

	/**
	 * Write data to the file on HDD
	 */
	public void saveConfig() {
		if (this.dataConfig == null || this.configFile == null)
			return;
		
		try {
			this.getConfig().save(this.configFile);
		} catch (IOException e) {
			plugin.getLogger().log(Level.SEVERE,"Could not save conftig to" + this.configFile,e);
		}
	}
	
	/**
	 * Créer le fichier openCite/npcs-data.yml
	 */
	public void saveDefaultConfig() {
		if (this.configFile == null)
			this.configFile = new File(this.plugin.getDataFolder(),"data/npcs-data.yml");
		if(!this.configFile.exists()) {
			this.plugin.saveResource("data/npcs-data.yml", false);
		}
	}
}
