package com.lamisedaxeh.npc;

import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R3.CraftServer;
import org.bukkit.craftbukkit.v1_16_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.lamisedaxeh.Main;
import com.lamisedaxeh.Utils;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import net.minecraft.server.v1_16_R3.EntityPlayer;
import net.minecraft.server.v1_16_R3.MinecraftServer;
import net.minecraft.server.v1_16_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_16_R3.PacketPlayOutEntityHeadRotation;
import net.minecraft.server.v1_16_R3.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_16_R3.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_16_R3.PlayerConnection;
import net.minecraft.server.v1_16_R3.PlayerInteractManager;
import net.minecraft.server.v1_16_R3.WorldServer;

public class NPC {
	@SuppressWarnings("unused")
	private static Main plugin;
	private static List<EntityPlayer> NPC = new ArrayList<EntityPlayer>();
	
	/**
	 * Command to create a NPC
	 * @param player player who execute the command
	 * @param name Name of NPC
	 * @param skin name of the skin to apply on the NPC
	 */
	public static void createNPC(Player player,String name, String skin, Main plugin) {
		MinecraftServer server = ((CraftServer) Bukkit.getServer()).getServer();
		WorldServer world = ((CraftWorld) Bukkit.getWorld(player.getWorld().getName())).getHandle();
		GameProfile gameProfile = new GameProfile(UUID.randomUUID(),Utils.chat(name));// MAX 16 char for color code + name !!!!
		
		EntityPlayer npc = new EntityPlayer(server, world, gameProfile, new PlayerInteractManager(world));
		
		npc.setLocation(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getBlockZ(), player.getLocation().getYaw(), player.getLocation().getPitch());
		
		String[] texture = getSkin(player,skin);
		gameProfile.getProperties().put("textures",new Property("textures", texture[0], texture[1]));
		
		addNPCPacket(npc,plugin);
		NPC.add(npc);
		
		/*SAVE DATA to file*/
		int var = 1;
		if(Main.getData().contains("data")) {
			var = Main.getData().getConfigurationSection("data").getKeys(false).size() + 1;
		}		
		Main.getData().set("data." + var + ".x", player.getLocation().getX());
		Main.getData().set("data." + var + ".y", player.getLocation().getY());
		Main.getData().set("data." + var + ".z", player.getLocation().getZ());
		Main.getData().set("data." + var + ".p", player.getLocation().getPitch());
		Main.getData().set("data." + var + ".yaw", player.getLocation().getYaw());
		Main.getData().set("data." + var + ".world", player.getLocation().getWorld().getName());
		Main.getData().set("data." + var + ".name", name);//TODO CHANGE ME !!!!
		Main.getData().set("data." + var + ".texture", texture[0]);
		Main.getData().set("data." + var + ".signature", texture[1]);
		Main.saveData();
	}
	
	/**
	 * Load NPC from data.yml
	 * @param location
	 * @param profile
	 */
	public static void loadNPC(Location location, GameProfile profile ,Main plugin) {
		MinecraftServer server = ((CraftServer) Bukkit.getServer()).getServer();
		WorldServer world = ((CraftWorld) location.getWorld()).getHandle();
		GameProfile gameProfile = profile;
		
		EntityPlayer npc = new EntityPlayer(server, world, gameProfile, new PlayerInteractManager(world));
		
		npc.setLocation(location.getX(), location.getY(), location.getBlockZ(), location.getYaw(), location.getPitch());
		
		addNPCPacket(npc,plugin);
		NPC.add(npc);

	}
	
	/**
	 * Get Texture of skin from mojang
	 * @param player
	 * @param name
	 * @return
	 */
	private static String[] getSkin(Player player, String name) {
		try {
			URL url = new URL("https://api.mojang.com/users/profiles/minecraft/" + name);
			InputStreamReader reader = new InputStreamReader(url.openStream());
			String uuid = new JsonParser().parse(reader).getAsJsonObject().get("id").getAsString();
			
			URL url2 = new URL("https://sessionserver.mojang.com/session/minecraft/profile/" + uuid + "?unsigned=false");
			InputStreamReader reader2 = new InputStreamReader(url2.openStream());
			JsonObject property = new JsonParser().parse(reader2).getAsJsonObject().get("properties").getAsJsonArray().get(0).getAsJsonObject();
			
			String texture = property.get("value").getAsString();
			String signature = property.get("signature").getAsString();
			
			return new String[] {texture, signature};
			
		} catch (Exception e) {
			EntityPlayer p = ((CraftPlayer) player).getHandle();
			GameProfile profile = p.getProfile();
			Property property = profile.getProperties().get("textures").iterator().next();
			
			String texture = property.getValue();
			String signature = property.getSignature();
			return new String[] {texture, signature};
		}
	}
	
	/**
	 * Create the NPC for each player online 
	 * @param npc
	 */
	public static void addNPCPacket(EntityPlayer npc, Main plugin) {
		for(Player player : Bukkit.getOnlinePlayers()) {
			PlayerConnection connection = ((CraftPlayer)player).getHandle().playerConnection;
			connection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, npc));
			connection.sendPacket(new PacketPlayOutNamedEntitySpawn(npc));
			connection.sendPacket(new PacketPlayOutEntityHeadRotation(npc,(byte) (npc.yaw * 256/360)));

			PacketPlayOutPlayerInfo packet3 = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, npc);
	        new BukkitRunnable() {
	            @Override
	        public void run() {
	            	connection.sendPacket(packet3);
	            }
	        }.runTaskLater(plugin,50);
		}
	}
	
	/**
	 * Create all the NPC for one player
	 * @param player
	 */
	public static void addJoinPacket(Player player,Main plugin) {
		for(EntityPlayer npc : NPC) {
			PlayerConnection connection = ((CraftPlayer)player).getHandle().playerConnection;
			connection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, npc));
			connection.sendPacket(new PacketPlayOutNamedEntitySpawn(npc));
			connection.sendPacket(new PacketPlayOutEntityHeadRotation(npc,(byte) (npc.yaw * 256/360)));

			PacketPlayOutPlayerInfo packet3 = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, npc);
	        new BukkitRunnable() {
	            @Override
	        public void run() {
	            	connection.sendPacket(packet3);
	            }
	        }.runTaskLater(plugin,50);
		}
	}

	/**
	 * @return list of all the NPC
	 */
	public static List<EntityPlayer> getNPC() {
		return NPC;
	}

	/**
	 * Remove a NPC for a player (usefull for reload a NPC)
	 * @param player 
	 * @param npc
	 */
	public static void removeNPC(Player player, EntityPlayer npc) {
		PlayerConnection connection = ((CraftPlayer)player).getHandle().playerConnection;
		connection.sendPacket(new PacketPlayOutEntityDestroy(npc.getId()));
	}
}
	