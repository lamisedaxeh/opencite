package com.lamisedaxeh.npc.Events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import net.minecraft.server.v1_16_R3.EntityPlayer;

/**
 * Event when a player right click on NPC 
 * @author lamisedaxeh
 */
public class RightClickNPC extends Event implements Cancellable{
	/**
	 * The player who right click the NPC
	 */
	private final Player player;
	/**
	 * The NPC rightclick by the player
	 */
	private final EntityPlayer npc;
	
	private boolean isCancelled;
	private static final HandlerList HANDLERS = new HandlerList();
	
	public RightClickNPC(Player player, EntityPlayer npc) {
		this.player = player;
		this.npc = npc;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public EntityPlayer getNpc() {
		return npc;
	}
		
	@Override
	public HandlerList getHandlers() {
		return HANDLERS;
	}
	
	public static HandlerList getHandlerList() {
		return HANDLERS;
	}
	
	@Override
	public boolean isCancelled() {
		return isCancelled;
	}

	@Override
	public void setCancelled(boolean arg) {
		isCancelled = arg;
	}
}
