package com.lamisedaxeh.npc.Events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.lamisedaxeh.ConfigData;
import com.lamisedaxeh.Utils;
import com.lamisedaxeh.gui.InventoryGui;
import com.lamisedaxeh.i18n.I18n2;

public class ClickNPC implements Listener{
	
	/**
	 * Function call when a player click on NPC
	 * @param event  
	 */
	@EventHandler
	public void onClick(RightClickNPC event) {
		Player player = event.getPlayer();
		if(player.hasPermission("opencite.player.event.interactNPC")) {
			if(InventoryGui.invs.keySet().contains(event.getNpc().getName())) {
				player.openInventory(InventoryGui.invs.get(event.getNpc().getName()));
				player.sendMessage(event.getNpc().getName() + ": " + Utils.chat("&r") + I18n2.gs("npc.t.NPCCatch"));
			}else { 		
				player.sendMessage(Utils.chat("&8[&6"+ConfigData.getCityName()+"&8] &7"+ I18n2.gs("log.t.NPCBug")));
			}
		}else {
			player.sendMessage(I18n2.gs("log.t.deniedPermission"));
			event.setCancelled(true);
		}		 
	}

}
