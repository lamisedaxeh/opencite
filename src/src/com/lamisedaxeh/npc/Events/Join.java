package com.lamisedaxeh.npc.Events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.lamisedaxeh.Main;
import com.lamisedaxeh.npc.NPC;
import com.lamisedaxeh.npc.PacketReader;
import com.lamisedaxeh.scoreboard.PersonalScoreboard;

public class Join implements Listener {
	private Main plugin;
	
	public Join(Main plugin) {
		this.plugin = plugin;
	}
	
	/**
	 * Load packet injection and inject all NPC to new connected player. 
	 * @param event
	 */
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		PersonalScoreboard.createBoard(event.getPlayer());
		PersonalScoreboard.start(event.getPlayer(), plugin);
		
		PacketReader reader = new PacketReader();
		reader.inject(event.getPlayer());
		
		/*If we have NPC load all the NPC*/
		if(NPC.getNPC() != null && !NPC.getNPC().isEmpty())
			NPC.addJoinPacket(event.getPlayer(),plugin);
	}
	
	/**
	 * Unload the packet injection
	 * @param event
	 */
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		PersonalScoreboard persoSB = new PersonalScoreboard(event.getPlayer().getUniqueId(), plugin);
		if(persoSB.hasID()) {
			persoSB.stop();
		}
		
		PacketReader reader = new PacketReader();
		reader.unInject(event.getPlayer());
	}
}