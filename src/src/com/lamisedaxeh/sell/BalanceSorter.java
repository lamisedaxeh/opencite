package com.lamisedaxeh.sell;

import java.util.Comparator;

public class BalanceSorter implements Comparator<Balance>{
	@Override
	public int compare(Balance o1, Balance o2) {
		return o1.getMoney().compareTo(o2.getMoney());
	}
}
