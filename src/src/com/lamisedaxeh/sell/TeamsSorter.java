package com.lamisedaxeh.sell;

import java.util.Comparator;

import com.lamisedaxeh.scoreboard.Team;

public class TeamsSorter implements Comparator<Team>{
	@Override
	public int compare(Team t1, Team t2) {
		return t1.getBalance().compareTo(t2.getBalance());
	}
}
