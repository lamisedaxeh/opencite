package com.lamisedaxeh.sell;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;

import org.bukkit.Material;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.lamisedaxeh.ConfigData;
import com.lamisedaxeh.Main;
import com.lamisedaxeh.Utils;
import com.lamisedaxeh.i18n.I18n2;
import com.lamisedaxeh.npc.NPCsFile;
import com.lamisedaxeh.scoreboard.Team;

public class Sell {
	private static Main plugin;
    private static File customConfigFile;
    private static FileConfiguration customConfig;
//TODO PATCH all customConfig.contains("teams") or customConfig.contains("teamsColors") qui ne marche plus
    
    public Sell(Main plugin) {
    	Sell.plugin = plugin;
    	createCustomConfig();
	}

    public FileConfiguration getCustomConfig() {
        return Sell.customConfig;
    }

    private void createCustomConfig() {
        customConfigFile = new File(plugin.getDataFolder(), "data/sell-data.yml");
        if (!customConfigFile.exists()) {
            customConfigFile.getParentFile().mkdirs();
            plugin.saveResource("data/sell-data.yml", false);
        }
        customConfig= new YamlConfiguration();
        try {
            customConfig.load(customConfigFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }
    

	/**
	 * Join team
	 * @param teamName
	 * @param player
	 * @return
	 */
	public static int teamLeave(String playerName) {
		//LOOP to find player by name
		if(customConfig.contains("players")) {
			for (String UUID2: customConfig.getConfigurationSection("players.").getKeys(false)) {
				String name = customConfig.getString("players."+UUID2+".name");
				if(name.equalsIgnoreCase(playerName)) {
					customConfig.set("players." + UUID2 + ".team", null);
					saveConfig();
					return 0;
				}
			}
			return 3;//Player does not exist
		}
		return 1;//Config file error
	}

	/**
	 * Join team
	 * @param teamName
	 * @param player
	 * @return
	 */
	public static int teamJoin(String teamName, String playerName) {
		if(ConfigData.getTeamsName().contains(teamName)){
			//LOOP to find player by name
			if(customConfig.contains("players")) {
				for (String UUID2: customConfig.getConfigurationSection("players.").getKeys(false)) {
					String name = customConfig.getString("players."+UUID2+".name");
					if(name.equalsIgnoreCase(playerName)) {
						customConfig.set("players." + UUID2 + ".team", teamName);
						saveConfig();
						return 0;
					}
				}
				return 3;//Player does not exist
			}
		}else {
			return 2;//TEAM does not exist
		}
		return 1; //Config file error
	}
	
	public static int sellItem(Material material,int quantite, Player player) {
		if(player.hasPermission("opencite.player.action.sellItem")) {
			int prixTot =0;
			ItemStack is = new ItemStack(material,quantite);
			if(player.getInventory().containsAtLeast(is, quantite)) {
				
				int itemsToRemove = quantite;
				
				for(ItemStack invItem : player.getInventory().getContents()) {
	                if(invItem != null && invItem.getType().equals(material)) {
	                    int preAmount = invItem.getAmount();
	                    int newAmount = Math.max(0, preAmount - itemsToRemove);
	                    
	                    itemsToRemove = Math.max(0, itemsToRemove - preAmount);
	                    invItem.setAmount(newAmount);
	                    
	                    if(itemsToRemove == 0) break;
	                }
	            }
				//player.sendMessage(quantite + " item vendu !");
				
				
				int key = 1;
				if(customConfig.contains("players." + player.getUniqueId() + ".ventes")) {
					key = customConfig.getConfigurationSection("players." + player.getUniqueId() + ".ventes").getKeys(false).size();
				}	
				
				int prixUnitaire = NPCsFile.getPrice(material.name());
				prixTot = prixUnitaire * quantite;
				
				customConfig.set("players." + player.getUniqueId() + ".name", player.getDisplayName());
				customConfig.set("players." + player.getUniqueId() + ".ventes." + key + ".item", material.name());
				customConfig.set("players." + player.getUniqueId() + ".ventes." + key + ".prixUnitaire", prixUnitaire);
				customConfig.set("players." + player.getUniqueId() + ".ventes." + key + ".quantite", quantite);			
				customConfig.set("players." + player.getUniqueId() + ".ventes." + key + ".prixtot", prixTot);
				
				saveConfig();
			}else{
				player.sendMessage(Utils.chat(
						"&8[&6"+ConfigData.getCityName()+"&8] &7"+ 
						I18n2.gs("npc.t.noItem")
			    ));
				return 0;
			}
			player.sendMessage(Utils.chat(
					"&8[&6"+ConfigData.getCityName()+"&8] &7"+ 
					String.format( 
							I18n2.gs("sel.t.sell"), 
							"&3" + quantite + " &4" + material.name() + "&7",
							"&e" + Utils.formatPrice(prixTot) +
						    " &6"+ ConfigData.getCoinSymbol())
		    ));
			return prixTot;
		}else {
			player.sendMessage(I18n2.gs("log.t.deniedPermission"));
			return 0;
		}		 
	}
	
	/**
	 * Save buy entry to the SELL DATA file
	 * @param type
	 * @param name
	 * @param price
	 * @param player
	 * @return true when the buying action achieve
	 */
	public static boolean buy(String type, String name ,int price, Player player) {
		if(player.hasPermission("opencite.player.action.buy")) {
			if(getBalance(player.getUniqueId().toString()) >= price) {
				int key = 1;
				if(customConfig.contains("players." + player.getUniqueId() + ".achat")) {
					key = customConfig.getConfigurationSection("players." + player.getUniqueId() + ".achat").getKeys(false).size();
				}
				
				customConfig.set("players." + player.getUniqueId() + ".name", player.getDisplayName());
				customConfig.set("players." + player.getUniqueId() + ".achat." + key + ".type",type);
				customConfig.set("players." + player.getUniqueId() + ".achat." + key + ".name", name);
				customConfig.set("players." + player.getUniqueId() + ".achat." + key + ".price", price);			
				saveConfig();
				return true;
			}
			return false;
		}else {
			player.sendMessage(I18n2.gs("log.t.deniedPermission"));
			return false;
		}
	}

	/**
	 * Get balance of a player
	 * @param UUID
	 * @return Total of coin
	 */
	public static int getBalance(String UUID) {
		int total = 0;
		if (customConfig.getConfigurationSection("players." + UUID +".ventes") != null) {
			for (String nbVente: customConfig.getConfigurationSection("players." + UUID +".ventes").getKeys(false)) {
				total+= customConfig.getInt("players." + UUID + ".ventes." + nbVente + ".prixtot");
			}
		}
		if (customConfig.getConfigurationSection("players." + UUID +".achat") != null) {
			for (String nbAchat: customConfig.getConfigurationSection("players." + UUID +".achat").getKeys(false)) {
				total-= customConfig.getInt("players." + UUID + ".achat." + nbAchat + ".price");
			}
		}
		return total;
	}
	
	/**
	 * Return allPlayer name with a their balance
	 * @return
	 */
	public static ArrayList<Balance> getAllPlayersBalance() {
		ArrayList<Balance> allBalance = new ArrayList<Balance>();
		for (String UUID: customConfig.getConfigurationSection("players.").getKeys(false)) {
			String name = customConfig.getString("players." + UUID + ".name");
			allBalance.add(new Balance(getBalance(UUID), name));			
		}
		return allBalance;
	}
	/**
	 * Return allTeam name with a their balance
	 * @return
	 */
	public static ArrayList<Team> getAllTeamsBalance() {
		ArrayList<Team> teams = ConfigData.getTeams();
		for (Team t : teams) {
			t.setBalance(getTeamBalance(t.getName()));
		}
		return teams;
	}
	
	/**
	 * Get balance of player team 
	 * @param UUID
	 * @return Total of coin
	 */
	public static int getTeamBalanceFromPlayer(Player player) {
		return getTeamBalance(getTeam(player));
	}
	
	/**
	 * Get team name of a player
	 * @param player
	 * @return 
	 */
	public static String getTeam(Player player) {
		String playerUUID = player.getUniqueId().toString();
		return customConfig.getString("players." + playerUUID + ".team");
	}
	

	
	/**
	 * Get balance of a team
	 * @param Team name 
	 * @return Total of coin
	 */
	public static int getTeamBalance(String name) {
		if(name == null || !ConfigData.getTeamsName().contains(name))
			return 0;//Zero si la team existe pas
			
			
		int total = 0;
		for (String UUID: customConfig.getConfigurationSection("players.").getKeys(false)) {
			if (customConfig.getConfigurationSection("players." + UUID +".ventes") != null) {
				for (String nbVente: customConfig.getConfigurationSection("players." + UUID +".ventes").getKeys(false)) {
					if(customConfig.getString("players." + UUID + ".team").equalsIgnoreCase(name)) {
						total+= customConfig.getInt("players." + UUID + ".ventes." + nbVente + ".prixtot");	
					}
				}
			}
			if (customConfig.getConfigurationSection("players." + UUID +".achat") != null) {
				for (String nbAchat: customConfig.getConfigurationSection("players." + UUID +".achat").getKeys(false)) {
					if(customConfig.getString("players." + UUID + ".team").equalsIgnoreCase(name)) {
						total-= customConfig.getInt("players." + UUID + ".achat." + nbAchat + ".price");
					}
				}
			}
		}
		return total;
	}
	
	

	//TODO refactor ALL function below :
	
	/**
	 * Write data to the file on HDD
	 */
	public static void saveConfig() {
		if (Sell.customConfig == null || Sell.customConfigFile == null)
			return;
		
		try {
			getConfig().save(Sell.customConfigFile);
		} catch (IOException e) {
			plugin.getLogger().log(Level.SEVERE,"Could not save conftig to" + Sell.customConfigFile,e);
		}
	}
	
	/**
	 * @return Actual config 
	 */
	public static FileConfiguration getConfig() {
		if (Sell.customConfig == null)
			reloadConfig();
		return Sell.customConfig;
	}

	/**
	 * Create fil or load data from exiting file
	 */
	private static void reloadConfig() {
		if (Sell.customConfigFile == null)
			Sell.customConfigFile = new File(plugin.getDataFolder(), "data/sell-data.yml");
		Sell.customConfig = YamlConfiguration.loadConfiguration(Sell.customConfigFile);
			
		InputStream defaultStream = plugin.getResource("data/sell-data.yml");
		if (defaultStream !=null) {
			YamlConfiguration defaultConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(defaultStream));
			Sell.customConfig.setDefaults(defaultConfig);
		}
	}
	
}
