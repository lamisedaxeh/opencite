package com.lamisedaxeh.sell;


public class Balance {
	private Integer money;
	private String name;

	public Balance(Integer money, String name) {
		super();
		this.money = money;
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Integer getMoney() {
		return money;
	}
	public void setMoney(Integer money) {
		this.money = money;
	}
}