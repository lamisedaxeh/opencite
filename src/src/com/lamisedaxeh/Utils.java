package com.lamisedaxeh;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class Utils {

	public static String chat(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}
	
	/**
	 * Create an item in inventory
	 * @param inv
	 * @param materialName
	 * @param amout
	 * @param invSlot
	 * @param displayName
	 * @param loreString
	 * @return The item
	 */
	public static ItemStack createItem(Inventory inv,String materialName,int amout,int invSlot,String displayName,String... loreString) {
		ItemStack item;
		List<String> lore = new ArrayList<String>();
		
		item = new ItemStack(Material.getMaterial(materialName),amout);//TODO VERIF toString
		
		ItemMeta meta = item.getItemMeta();
		if (displayName != null)
			meta.setDisplayName(Utils.chat(displayName));
		for(String s : loreString) {
			lore.add(chat(s));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		
		inv.setItem(invSlot, item);
		return item;
	}
	
	/**
	 * Format int to printable price
	 * @param price 
	 * @return formated string
	 */
	public static String formatPrice(int price) {
		return NumberFormat.getNumberInstance(Locale.US).format(price).replace(',',' ');
	}
}
