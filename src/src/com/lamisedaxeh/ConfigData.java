package com.lamisedaxeh;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.lamisedaxeh.Main;
import com.lamisedaxeh.Utils;
import com.lamisedaxeh.gui.InventoryGui;
import com.lamisedaxeh.scoreboard.Team;
import com.lamisedaxeh.sell.Sell;

import net.md_5.bungee.api.ChatColor;

public class ConfigData {
	private static Main plugin;
    private static File customConfigFile;
    private static FileConfiguration customConfig;

    
    
    public ConfigData(Main plugin) {
		ConfigData.plugin = plugin;
		createCustomConfig();

	}


//	public FileConfiguration getCustomConfig() {
//        return ConfigData.customConfig;
//    }
	
	/**
	 * Return the language in the config file
	 * @return
	 */
	public static String getLang() {
		return customConfig.getString("lang");
	}
	
	/**
	 * Return the coin name from the config file
	 * @return
	 */
	public static String getCoinName() {
		return customConfig.getString("coinName");
	}
	
	/**
	 * Return the coin symbol from the config file
	 * @return
	 */
	public static String getCoinSymbol() {
		return customConfig.getString("coinSymbol");
	}

	/**
	 * Return the name of the city from the config file
	 * @return
	 */
	public static String getCityName() {
		return customConfig.getString("cityName");
	}

	/**
	 * Get all team and team color
	 * @return all teams
	 */
	public static ArrayList<Team> getTeams() {
		ArrayList<Team> result =  new ArrayList<>();
		List<String> teamsName = customConfig.getStringList("teams");
		List<String> teamsColor= customConfig.getStringList("teamsColors");
		
		for(int i=0 ; i < teamsName.size() ; i++) {
			result.add(new Team(teamsName.get(i),teamsColor.get(i)));
		}
		return result;
	}
	
	/**
	 * replace me with getTeams()
	 * @return all teams
	 */
	@Deprecated  
	public static List<String> getTeamsName() {
		return customConfig.getStringList("teams");
	}
	
    /**
     * Create new team
     * @param name Team name
     * @param color Team color in MC format &1
     * @return error code
     */
	public static int teamCreate(String name, String color) {
		if(customConfig.contains("teams") && customConfig.contains("teamsColors")) {
			List<String> teams = customConfig.getStringList("teams");
			List<String> teamsColors = customConfig.getStringList("teamsColors");
			
			if(teams.contains(name)) {
				return 2;//ERROR TEAM NAME ALREADY EXIST
			}else if(teamsColors.contains(color)) {
				return 3;//ERROR TEAM COLOR ALREADY EXIST
			}
			
			teams.add(name);
			teamsColors.add(color);
			
			customConfig.set("teams", teams);
			customConfig.set("teamsColors", teamsColors);
			
			saveConfig();
			
			return 0;
		}
		return 1;//Invalid config file
	}
	
	
    private void createCustomConfig() {
        customConfigFile = new File(plugin.getDataFolder(), "config.yml");
        if (!customConfigFile.exists()) {
            customConfigFile.getParentFile().mkdirs();
            plugin.saveResource("config.yml", false);
         }
        
        customConfig= new YamlConfiguration();
        try {
            customConfig.load(customConfigFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }	
    
    
    
	//TODO refactor ALL function below :
	
	/**
	 * Write data to the file on HDD
	 */
	public static void saveConfig() {
		if (ConfigData.customConfig == null || ConfigData.customConfigFile == null)
			return;
		
		try {
			getConfig().save(ConfigData.customConfigFile);
		} catch (IOException e) {
			plugin.getLogger().log(Level.SEVERE,"Could not save conftig to" + ConfigData.customConfigFile,e);
		}
	}
	
	/**
	 * @return Actual config 
	 */
	public static FileConfiguration getConfig() {
		if (ConfigData.customConfig == null)
			reloadConfig();
		return ConfigData.customConfig;
	}

	/**
	 * Create file or load data from exiting file
	 */
	private static void reloadConfig() {
		if (ConfigData.customConfigFile == null)
			ConfigData.customConfigFile = new File(plugin.getDataFolder(), "config.yml");
		ConfigData.customConfig = YamlConfiguration.loadConfiguration(ConfigData.customConfigFile);
			
		InputStream defaultStream = plugin.getResource("config.yml");
		if (defaultStream !=null) {
			YamlConfiguration defaultConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(defaultStream));
			ConfigData.customConfig.setDefaults(defaultConfig);
		}
	}
}
