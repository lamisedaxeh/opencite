package com.lamisedaxeh.scoreboard;

public class Team {

	/**
	 * Team color Code
	 */
	private String color;
	/**
	 * Team Name
	 */
	private String name;
	
	private Integer balance = 0;

	public Team(String name,String color) {
		super();
		this.color = color;
		this.name = name;
	}
	
	public Team(String color, String name,int balance) {
		super();
		this.color = color;
		this.name = name;
		this.balance = balance;
	}

	/**
	 * Return team color code
	 * @return
	 */
	public String getColor() {
		return color;
	}

	/**
	 * Return team name
	 * @return
	 */
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}


	public Integer getBalance() {
		return balance;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	
}
