package com.lamisedaxeh.scoreboard;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import com.lamisedaxeh.ConfigData;
import com.lamisedaxeh.Main;
import com.lamisedaxeh.Utils;
import com.lamisedaxeh.i18n.I18n2;
import com.lamisedaxeh.sell.Sell;

import net.md_5.bungee.api.ChatColor;

public class PersonalScoreboard {
	@SuppressWarnings("unused")
	private Main plugin;
	
	private static Map<UUID,Integer> TASKS = new HashMap<UUID, Integer>();
	private UUID uuid;
	
	private static int taskID;
	
	public PersonalScoreboard(UUID uuid,Main plugin) {
		this.plugin = plugin;
		this.uuid = uuid;
	}

	public UUID getID() {
		return uuid;
	}

	public void setID(int id) {
		TASKS.put(uuid, id);
	}
	
	public boolean hasID() {
		if(TASKS.containsKey(uuid))
			return true;
		return false;
	}
	
	public void stop() {
		Bukkit.getScheduler().cancelTask(TASKS.get(uuid));
		TASKS.remove(uuid);

	}
	
	public static void start(Player player, Main plugin) {
		taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			int count = 0;
			PersonalScoreboard board = new PersonalScoreboard(player.getUniqueId(), plugin);
			
			@Override
			public void run() {
				if (!board.hasID())
					board.setID(taskID);

				if (count > 1) {
					count = 0;
				}
				
				switch (count) {
					case 0:
						player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplayName(Utils.chat("&a&l<< &4&l"+ ConfigData.getCityName() +" &a&l>>"));
						break;
					case 1: 
						player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplayName(Utils.chat("&a&l<< &2&l"+ ConfigData.getCityName() +" &a&l>>"));
						createBoard(player);
						break;
					default:
						throw new IllegalArgumentException("Unexpected value: " + count);
				}
				count++;
			}
		}, 0, 10);
	}
	
	public static void createBoard(Player player) {
		ScoreboardManager manager =Bukkit.getScoreboardManager();
		Scoreboard board = manager.getNewScoreboard();
		Objective obj = board.registerNewObjective("persoSB","dummy",Utils.chat("&a&l<< &2&l"+ ConfigData.getCityName() +" &a&l>>"));
		
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		Score score = obj.getScore(ChatColor.BLUE + "=-=-=-=-=-=-=-=-=");
		score.setScore(5);

		Score score1 = obj.getScore(ChatColor.AQUA + Sell.getTeam(player) + " "+ ConfigData.getCoinName() + " : ");
		score1.setScore(4);
		Score score2 = obj.getScore(ChatColor.RESET + "" + ChatColor.YELLOW + "    " + Utils.formatPrice(Sell.getTeamBalanceFromPlayer(player)) + Utils.chat(" &6" + ConfigData.getCoinSymbol()));
		score2.setScore(3);
		
		Score score3 = obj.getScore(ChatColor.AQUA + I18n2.gs("sco.s.your") + " " + ConfigData.getCoinName() + " : ");
		score3.setScore(2);
		Score score4 = obj.getScore(ChatColor.YELLOW + "    " +Utils.formatPrice(Sell.getBalance(player.getUniqueId().toString())) + Utils.chat(" &6" + ConfigData.getCoinSymbol()));
		score4.setScore(1);

		Score score5 = obj.getScore(ChatColor.BLACK + "" + ChatColor.BLUE + "-=-=-=-=-=-=-=-=-");
		score5.setScore(0);
		player.setScoreboard(board);
	}
	
	
}
