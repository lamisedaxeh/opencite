package com.lamisedaxeh;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.lamisedaxeh.gui.InventoryGui;
import com.lamisedaxeh.holoDisplay.HoloData;
import com.lamisedaxeh.holoDisplay.Holograms;
import com.lamisedaxeh.i18n.I18n2;
import com.lamisedaxeh.npc.NPC;
import com.lamisedaxeh.npc.NPCsFile;
import com.lamisedaxeh.plot.PlotData;
import com.lamisedaxeh.sell.Sell;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.RegionContainer;

import net.md_5.bungee.api.ChatColor;

public class Commands implements CommandExecutor {
	private Main plugin;
	
	public Commands(Main plugin) {
		this.plugin = plugin;
		plugin.getCommand("spawnnpc").setExecutor(this);
		plugin.getCommand("trade").setExecutor(this);
		plugin.getCommand("getTeams").setExecutor(this);
		plugin.getCommand("getBalance").setExecutor(this);
		plugin.getCommand("getTeamBalance").setExecutor(this);
		plugin.getCommand("buyPLot").setExecutor(this);
		plugin.getCommand("getPLots").setExecutor(this);
		plugin.getCommand("createPLot").setExecutor(this);
		plugin.getCommand("addMember").setExecutor(this);
		plugin.getCommand("spawnscoreboard").setExecutor(this);
		plugin.getCommand("removescoreboard").setExecutor(this);
		plugin.getCommand("teamCreate").setExecutor(this);
		plugin.getCommand("teamJoin").setExecutor(this);
		plugin.getCommand("teamLeave").setExecutor(this);
	}
	
	/**
	 * Command management
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(I18n2.gs("log.t.needPlayer"));
			return true;
		}
		Player player = (Player) sender;
	
		if(label.equalsIgnoreCase("spawnscoreboard")) {
			
			if(args.length != 1)
			{
				player.sendMessage(I18n2.gs("log.t.need1Arg"));
			}else {
				if("player".equalsIgnoreCase(args[0])) {
					HoloData.saveHolo("players",
							player.getLocation().getWorld().getName(),
							player.getLocation().getX(),
							player.getLocation().getY(),
							player.getLocation().getZ());
					return Holograms.spawnPlayerSB(player.getLocation(),plugin);
				}else if ("team".equalsIgnoreCase(args[0])) {
					HoloData.saveHolo("teams",
							player.getLocation().getWorld().getName(),
							player.getLocation().getX(),
							player.getLocation().getY(),
							player.getLocation().getZ());
					return Holograms.spawnTeamSB(player.getLocation(),plugin);
				}
				
			}
			
			return false;


		}
		
		if(label.equalsIgnoreCase("removescoreboard")) {
			HoloData.removeHolo(player.getLocation());
			Holograms.reloadHolo();
			Holograms.removeHolo(player.getLocation());
			player.sendMessage(I18n2.gs("log.t.HologramRemove"));
			return true;
		}
		
		if(label.equalsIgnoreCase("buyPlot")) {
			if(args.length != 1)
			{
				player.sendMessage(I18n2.gs("log.t.need1Arg"));
			}else {
				if(PlotData.buyPlot(args[0], player)) {
					player.sendMessage(I18n2.gs("log.t.plotBuy"));				
				}else {
					player.sendMessage(I18n2.gs("log.t.noMoney"));
				}
				return true;
			}
			return false;
		}
		if(label.equalsIgnoreCase("createPlot")) {
			if(args.length != 2)
			{
				player.sendMessage(I18n2.gs("log.t.need2Arg"));
			}else {
				if(PlotData.createPlot(args[0], args[1])) {
					player.sendMessage(I18n2.gs("log.t.plotCreated"));
					return true;
				}
			}
			return false;
		}
		if(label.equalsIgnoreCase("getPlots")) {
			player.sendMessage(PlotData.getPlots().toString());
			return true;
		}		
		
		if(label.equalsIgnoreCase("addMember")) {
			if(args.length != 2)
			{
				player.sendMessage(I18n2.gs("log.t.need2Arg"));
			}else {
				RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
				RegionManager regions = container.get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
				
				if(regions.hasRegion(args[0])) {
					if(regions.getRegion(args[0]).isOwner(WorldGuardPlugin.inst().wrapPlayer(player))){
						regions.getRegion(args[0]).getMembers().addPlayer(args[1]);
						player.sendMessage(String.format( I18n2.gs("log.t.newMember"), args[1], args[0]));
					}else {
						player.sendMessage(String.format(I18n2.gs("log.t.notOwner"), args[0]));
						return false;	
					}
				}else {
					player.sendMessage(String.format(I18n2.gs("log.t.noRegion"), args[0]));
					return false;
				}
				
				return true;
			}
			return false;
		}
		
		if(label.equalsIgnoreCase("getTeams")) {
			player.sendMessage(ConfigData.getTeamsName().toString());
			return true;
		}
		if(label.equalsIgnoreCase("getBalance")) {
			player.sendMessage(Integer.toString(Sell.getBalance(player.getUniqueId().toString())));
			return true;
		}
		if(label.equalsIgnoreCase("getTeamBalance")) {
			if(args.length != 1)
			{
				player.sendMessage(I18n2.gs("log.t.need1Arg"));
			}else {
				player.sendMessage(Integer.toString(Sell.getTeamBalance(args[0])));
			}
			return true;
		}
		
		
		
		//Create an NPC if the name < 17
		if(label.equalsIgnoreCase("spawnnpc")) {
			if(args.length != 1)
			{
				player.sendMessage(I18n2.gs("log.t.need1Arg"));
			}else {
				if (args[0].length() < 17) {
					
					String realName = NPCsFile.getRealName(args[0]);
					if (realName != null) {
						NPC.createNPC(player, realName,NPCsFile.getSkinName(realName),plugin);
					}else {
						player.sendMessage(I18n2.gs("log.t.noPNJ"));
						return true;	
					}
				}else if (args[0].length() > 16) {
					player.sendMessage(I18n2.gs("log.t.16charMax"));
					return true;	
				}		
				player.sendMessage(I18n2.gs("log.t.NPCCreated"));
				return true;
			}
			
			
			
		}
		
		
		
		//CreateTEAM
		if(label.equalsIgnoreCase("teamCreate")) {
			if(args.length != 2){
				player.sendMessage(I18n2.gs("log.t.need2Arg"));
			}else {
				int r = ConfigData.teamCreate(args[0], args[1]);
				if(r==0) {
					player.sendMessage(String.format(I18n2.gs("log.t.teamCreated"), Utils.chat(args[1] + args[0] + "&r")));
					return true;
				}else if(r==2) {
					player.sendMessage(I18n2.gs("log.t.teamAsSameName"));
				}else if (r == 3) {
					player.sendMessage(I18n2.gs("log.t.teamAsSameColor"));
				}else {
					player.sendMessage(I18n2.gs("log.t.invalidConfigFileTeams"));
				}
			}
			return false;
		}
		
		//Join Team
		if(label.equalsIgnoreCase("teamJoin")) {
			if(args.length != 2){
				player.sendMessage(I18n2.gs("log.t.need2Arg"));
			}else {
				int r = Sell.teamJoin(args[1], args[0]);
				if(r==0) {
					player.sendMessage(String.format(I18n2.gs("log.t.playerAddToTeam"), args[0], Utils.chat(args[1] +"&r")));
					return true;
				}else if(r==2) {
					player.sendMessage(I18n2.gs("log.t.noTeam"));
				}else if (r == 3) {
					player.sendMessage(I18n2.gs("log.t.noPlayer"));
				}else {
					player.sendMessage(I18n2.gs("log.t.noConf"));
				}
			}
			return false;
		}
		
		//Join Team
		if(label.equalsIgnoreCase("teamLeave")) {
			if(args.length != 1){
				player.sendMessage(I18n2.gs("log.t.need1Arg"));
			}else {
				int r = Sell.teamLeave(args[0]);
				if(r==0) {
					player.sendMessage(String.format(I18n2.gs("log.t.playerRemoveToTeam"), args[0]));
					return true;
				}else if (r == 3) {
					player.sendMessage(I18n2.gs("log.t.noPlayer"));
				}else {
					player.sendMessage(I18n2.gs("log.t.noConf"));
				}
			}
			return false;
		}
		
		
		
		
		
		
		
		//Open a trade Gui
		if(label.equalsIgnoreCase("trade")) {
			if(args.length != 1)
			{
				player.sendMessage(I18n2.gs("log.t.need1Arg"));
			}else {
				for (String s : InventoryGui.invs.keySet()) {
					if(ChatColor.stripColor(s).equalsIgnoreCase(args[0])) {
						player.openInventory(InventoryGui.invs.get(s));
						return true;
					}
				}
				player.sendMessage(I18n2.gs("log.t.noTrade"));
			}
		}
		return false;
	}
}
