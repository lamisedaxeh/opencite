package com.lamisedaxeh.holoDisplay;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import com.lamisedaxeh.ConfigData;
import com.lamisedaxeh.Main;
import com.lamisedaxeh.Utils;
import com.lamisedaxeh.i18n.I18n2;
import com.lamisedaxeh.scoreboard.Team;
import com.lamisedaxeh.sell.Balance;
import com.lamisedaxeh.sell.BalanceSorter;
import com.lamisedaxeh.sell.Sell;
import com.lamisedaxeh.sell.TeamsSorter;

public class Holograms {
	private static ArrayList<Integer>  taskID = new ArrayList<Integer>();
	
	public static boolean spawnPlayerSB(Location location, Main plugin) {
		World world = location.getWorld();
		
		taskID.add(Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			@Override
			public void run() {
				removeHolo(location);
				Location l = new Location(location.getWorld(),location.getX(),location.getY(),location.getZ(),location.getYaw(),location.getPitch());
				ArmorStand hologram = (ArmorStand) world.spawnEntity(l.add(0, 0, 0),EntityType.ARMOR_STAND);
				

				hologram.setVisible(false);
				hologram.setCustomNameVisible(true);
				hologram.setCustomName(Utils.chat("&9&l────── &f&l"+ I18n2.gs("hol.h.player") +" &9&l──────"));
				hologram.setGravity(false);
				
				ArrayList<Balance> allBalance = Sell.getAllPlayersBalance();
				Collections.sort(allBalance,new BalanceSorter());
				int numberOfBalance = allBalance.size();
				
				for(int i = 0; i < 10 && i < numberOfBalance;i++) {
					Balance playerBalance = allBalance.remove(allBalance.size()-1);
					ArmorStand hologram3 = (ArmorStand) world.spawnEntity(l.add(0, -0.3, 0),EntityType.ARMOR_STAND);
					hologram3.setVisible(false);
					hologram3.setCustomNameVisible(true);
					//One line format
					hologram3.setCustomName(Utils.chat(
							"&9" + 
							(i+1) + 
							"&7. &r" + 
							playerBalance.getName() + 
							"&f: &e&l"+ 
							Utils.formatPrice(playerBalance.getMoney())+ 
							" &6"+
							ConfigData.getCoinSymbol())
					);
					hologram3.setGravity(false);
				}
			
				ArmorStand hologram4 = (ArmorStand) world.spawnEntity(l.add(0, -0.3, 0),EntityType.ARMOR_STAND);
				hologram4.setVisible(false);
				hologram4.setCustomNameVisible(true);
				hologram4.setCustomName(Utils.chat("&9────────────────────"));
				hologram4.setGravity(false);
				
				ArmorStand hologram5 = (ArmorStand) world.spawnEntity(l.add(0, -0.3, 0),EntityType.ARMOR_STAND);
				hologram5.setVisible(false);
				hologram5.setCustomNameVisible(true);
				hologram5.setCustomName(Utils.chat("&7Plugin &6OpenCité &7by &9Lamisedaxeh"));
				hologram5.setGravity(false);
				
				
			}
		}, 0, 200));
		return true;
	}
	
	public static boolean spawnTeamSB(Location location, Main plugin) {
		World world = location.getWorld();
		
		taskID.add(Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			@Override
			public void run() {
				//Clean and create new holo
				removeHolo(location);
				Location l = new Location(location.getWorld(),location.getX(),location.getY(),location.getZ(),location.getYaw(),location.getPitch());
				ArmorStand hologram = (ArmorStand) world.spawnEntity(l.add(0, 0, 0),EntityType.ARMOR_STAND);
				
				//printe header
				hologram.setVisible(false);
				hologram.setCustomNameVisible(true);
				hologram.setCustomName(Utils.chat("&9&l────── &f&l"+ I18n2.gs("hol.h.teams") +" &9&l──────"));
				hologram.setGravity(false);

				//Sort team by balance 
				ArrayList<Team> teamsBalance = Sell.getAllTeamsBalance();
				Collections.sort(teamsBalance,new TeamsSorter());
				int numberOfTeam = teamsBalance.size();
				
				for(int i = 0; i < 10 && i < numberOfTeam;i++) {
					Team team = teamsBalance.remove(teamsBalance.size()-1);

					ArmorStand hologram3 = (ArmorStand) world.spawnEntity(l.add(0, -0.3, 0),EntityType.ARMOR_STAND);
					hologram3.setVisible(false);
					hologram3.setCustomNameVisible(true);
					//One line format
					hologram3.setCustomName(Utils.chat(
							"&9" + 
							(i+1) + 
							"&7. &r" + team.getColor() + 
							team.getName() + 
							"&f: &e&l"+ 
							Utils.formatPrice(team.getBalance())+ 
							" &6"+
							ConfigData.getCoinSymbol())
					);
					hologram3.setGravity(false);
				}
				ArmorStand hologram4 = (ArmorStand) world.spawnEntity(l.add(0, -0.3, 0),EntityType.ARMOR_STAND);
				hologram4.setVisible(false);
				hologram4.setCustomNameVisible(true);
				hologram4.setCustomName(Utils.chat("&9────────────────────"));
				hologram4.setGravity(false);
				
				ArmorStand hologram5 = (ArmorStand) world.spawnEntity(l.add(0, -0.3, 0),EntityType.ARMOR_STAND);
				hologram5.setVisible(false);
				hologram5.setCustomNameVisible(true);
				hologram5.setCustomName(Utils.chat("&7Plugin &6OpenCité &7by &9Lamisedaxeh"));
				hologram5.setGravity(false);
				
				
			}
		}, 0, 200));
		return true;
	}
	
	
	
	
	//TODO cleanup -> to no static ?
	public static void reloadHolo() {
		for (Integer task : taskID) {
			Bukkit.getServer().getScheduler().cancelTask(task);
		}
		HoloData.loadHolo();
	}
	
	public static void removeHolo(Location location) {
		for(Entity entity:location.getWorld().getEntities()){
			if(entity.getType().equals(EntityType.ARMOR_STAND)){
				if(entity.getLocation().getBlockX()==location.getBlockX()){
					if(entity.getLocation().getBlockZ()==location.getBlockZ()){
						entity.remove();
					}
				}
			}
	    }
	}
}
