package com.lamisedaxeh.holoDisplay;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.InvalidConfigurationException;

import com.lamisedaxeh.Main;

public class HoloData {

		private static Main plugin;
	    private static File customConfigFile;
	    private static FileConfiguration customConfig;

	    
	    public HoloData(Main plugin) {
	    	HoloData.plugin = plugin;
	    	createCustomConfig();
	    	loadHolo();
		}

	    public FileConfiguration getCustomConfig() {
	        return HoloData.customConfig;
	    }

	    private void createCustomConfig() {
	        customConfigFile = new File(plugin.getDataFolder(), "data/holo-data.yml");
	        if (!customConfigFile.exists()) {
	            customConfigFile.getParentFile().mkdirs();
	            plugin.saveResource("data/holo-data.yml", false);
	        }
	        customConfig= new YamlConfiguration();
	        try {
	            customConfig.load(customConfigFile);
	        } catch (IOException | InvalidConfigurationException e) {
	            e.printStackTrace();
	        }
	    }
	    
	    public static void loadHolo() {
	    	for (String holoId: customConfig.getConfigurationSection("hologrammes").getKeys(false)) {
	    		String type = customConfig.getString("hologrammes." + holoId + ".type");
	    		String world = customConfig.getString("hologrammes." + holoId + ".world");
	    		Double x = customConfig.getDouble("hologrammes." + holoId + ".x");
	    		Double y = customConfig.getDouble("hologrammes." + holoId + ".y");
	    		Double z = customConfig.getDouble("hologrammes." + holoId + ".z");
	    		if(type.equalsIgnoreCase("players"))
	    			Holograms.spawnPlayerSB(new Location(Bukkit.getWorld(world), x, y, z), plugin);
	    		else if(type.equalsIgnoreCase("teams"))
	    			Holograms.spawnTeamSB(new Location(Bukkit.getWorld(world), x, y, z), plugin);
	    	}
		}
	    
	    public static boolean saveHolo(String type,String world,Double x,Double y,Double z) {
			int holoId = 1;
			if(customConfig.contains("hologrammes")) {
				holoId = customConfig.getConfigurationSection("hologrammes").getKeys(false).size() + 1;
			}
			
    		customConfig.set("hologrammes." + holoId + ".type",type);
    		customConfig.set("hologrammes." + holoId + ".world",world);
    		customConfig.set("hologrammes." + holoId + ".x",x);
    		customConfig.set("hologrammes." + holoId + ".y",y);
    		customConfig.set("hologrammes." + holoId + ".z",z);
    		saveConfig();
	    	return true;
	    }
	    
		public static void removeHolo(Location location) {
			for (String holoId: customConfig.getConfigurationSection("hologrammes").getKeys(false)) {
	    		String world = customConfig.getString("hologrammes." + holoId + ".world");
	    		Double x = customConfig.getDouble("hologrammes." + holoId + ".x");
	    		Double y = customConfig.getDouble("hologrammes." + holoId + ".y");
	    		Double z = customConfig.getDouble("hologrammes." + holoId + ".z");
	    		Location l = new Location(Bukkit.getWorld(world), x, y, z);
	    		if(l.getBlockX() == location.getBlockX() && l.getBlockZ() == location.getBlockZ()){
	    			customConfig.set("hologrammes." + holoId, null);
	    		}
			}
			saveConfig();
			
		}
	    

	    //TODO refactor ALL function below :
		
		/**
		 * Write data to the file on HDD
		 */
		public static void saveConfig() {
			if (HoloData.customConfig == null || HoloData.customConfigFile == null)
				return;
			
			try {
				getConfig().save(HoloData.customConfigFile);
			} catch (IOException e) {
				plugin.getLogger().log(Level.SEVERE,"Could not save conftig to" + HoloData.customConfigFile,e);
			}
		}
		
		/**
		 * @return Actual config 
		 */
		public static FileConfiguration getConfig() {
			if (HoloData.customConfig == null)
				reloadConfig();
			return HoloData.customConfig;
		}

		/**
		 * Create file or load data from exiting file
		 */
		private static void reloadConfig() {
			if (HoloData.customConfigFile == null)
				HoloData.customConfigFile = new File(plugin.getDataFolder(), "data/holo-data.yml");
			HoloData.customConfig = YamlConfiguration.loadConfiguration(HoloData.customConfigFile);
				
			InputStream defaultStream = plugin.getResource("data/holo-data.yml");
			if (defaultStream !=null) {
				YamlConfiguration defaultConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(defaultStream));
				HoloData.customConfig.setDefaults(defaultConfig);
			}
		}
		

}
