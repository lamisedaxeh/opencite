package com.lamisedaxeh.gui.Events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.lamisedaxeh.Main;
import com.lamisedaxeh.gui.InventoryGui;

public class InventoryClickListener implements Listener{
	//private Main plugin;
	
	public InventoryClickListener(Main plugin) {
		//this.plugin = plugin;
		Bukkit.getPluginManager().registerEvents(this,plugin);
	}
	
	@EventHandler
	public static void onClick(InventoryClickEvent e) {
		String title = e.getView().getTitle();
		
		for (String inventory_names : InventoryGui.invs.keySet()) {
			if(title.equals(inventory_names)) {
				e.setCancelled(true);
				if(e.getCurrentItem() == null) {
					return;
				}
				InventoryGui.clicked((Player) e.getWhoClicked(), e.getSlot(), e.getCurrentItem(), e.getInventory(), title);
				return;
			}
		}
		
	}
	
	
}
