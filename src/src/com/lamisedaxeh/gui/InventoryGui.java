package com.lamisedaxeh.gui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.lamisedaxeh.ConfigData;
import com.lamisedaxeh.Utils;
import com.lamisedaxeh.i18n.I18n2;
import com.lamisedaxeh.sell.Sell;



public class InventoryGui {
	

	public static Map<String, Inventory> invs = new HashMap <String,Inventory>();// inventory_names,inv
	public int inv_row = 6 * 9;
	public String inventory_name;
	
	/**
	 * Create the inventory
	 * @param name Nom de l'inventaire
	 * @param row Nombre de ligne
	 */
	public InventoryGui(String name,int row) {
		this.inventory_name = Utils.chat(name);
		this.inv_row = row * 9;
		
		invs.put(name,Bukkit.createInventory(null, inv_row, name));
	}
	
	/**
	 * Add an item to the inventory
	 * @param material
	 * @param slot
	 * @param displayName set null to get the default name
	 * @param loreString
	 */
	public void addItem(String material, int slot,String displayName ,String... loreString) {
		Utils.createItem(invs.get(inventory_name), material, 1, slot, displayName, loreString);//
	}
	
	/**
	 * Generate the GUI
	 * @return
	 */
	public Inventory GUI() {
		Inventory toReturn = Bukkit.createInventory(null, inv_row,inventory_name);
		toReturn.setContents(invs.get(inventory_name).getContents());
		return toReturn;
	}
	
	/**
	 * Called when an item is clicked
	 * @param player
	 * @param slot
	 * @param clicked
	 * @param inv
	 */
	public static void clicked(Player player, int slot, ItemStack clicked,Inventory inv, String invName) {
		if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase(Utils.chat("&7"+I18n2.gs("gui.i.separator")))){
			return;
		}
		if (invName.equalsIgnoreCase(Utils.chat("&3" + I18n2.gs("gui.n.quantity")))) {

			
			List<String> lore = clicked.getItemMeta().getLore();
			
			if(lore.get(0).contains(I18n2.gs("gui.l.sellOne")))

				Sell.sellItem(clicked.getType(), 1, player);
			if(lore.get(0).contains(I18n2.gs("gui.l.sellTen")))

				Sell.sellItem(clicked.getType(), 10, player);
			if(lore.get(0).contains(I18n2.gs("gui.l.sellAll"))) {
				int nbItem = 0;
		        for(ItemStack item : player.getInventory().getContents()) {
		            if(item != null && item.getType().equals(clicked.getType())) {
		            	nbItem+= item.getAmount();
		            }
		        }
		        Sell.sellItem(clicked.getType(), nbItem, player);
			}				
			player.closeInventory();
		}else {
			
			
			InventoryGui invGui = new InventoryGui(Utils.chat("&3"+I18n2.gs("gui.n.quantity")),3);
			for(int i =0; i<=8;i++) {
				invGui.addItem("BLACK_STAINED_GLASS_PANE", i, Utils.chat("&7"+I18n2.gs("gui.i.separator")));	
			}
			for(int i =0; i<=8;i++) {
				invGui.addItem("BLACK_STAINED_GLASS_PANE", i+18, Utils.chat("&7"+I18n2.gs("gui.i.separator")));	
			}
			invGui.addItem(clicked.getType().name(), 11,null , I18n2.gs("gui.l.sellOne"));
			invGui.addItem(clicked.getType().name(), 13,null , I18n2.gs("gui.l.sellTen"));
			invGui.addItem(clicked.getType().name(), 15,null , I18n2.gs("gui.l.sellAll"));
			
			player.openInventory(invGui.GUI());
		}
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
